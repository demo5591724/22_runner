#!/bin/bash

# Install GitLab Runner
if ! command -v gitlab-runner &> /dev/null
then
  echo "GitLab Runner is not installed. Installing..."
  curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
  chmod +x /usr/local/bin/gitlab-runner
  echo "GitLab Runner installed successfully."
else
  echo "GitLab Runner is already installed."
fi

# Register GitLab Runner
export GITLAB_URL="https://gitlab.com/"
export REGISTRATION_TOKEN="your-registration-token"
export ACCESS_TOKEN="admin/system"

sudo gitlab-runner register \
  --non-interactive \
  --url "$GITLAB_URL" \
  --registration-token "$REGISTRATION_TOKEN" \
  --name "Netcloud-Deployment" \
  --executor "shell" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-token "$ACCESS_TOKEN" \
  --description "GitLab Runner for your project" \
  --request-concurrency="1" \
  --docker-image="alpine:latest"
